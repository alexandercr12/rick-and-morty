# Rick and Morty App

Proyecto para la gestión de personajes favoritos del programa de televisión Rick and Morty (prueba técnica Suazo.co)

## Recursos

Prototipos: https://www.figma.com/file/6GN6h4XsZuC6z1zDv5Yhxa/Inicio?node-id=1%3A12  
API REST: https://rickandmortyapi.com/documentation/#rest

## Stack

NodeJs: 14.17.1  
Npm: 6.14.13  
Vuejs(vue@next): 3.1.5  
@vue/cli: 4.5.13  
quasar: 2.0.0  
axios: 0.21.1  
vue-jest: 5.0.0-0  
testing-library/vue: 5.8.2  
jest: 27.0.6  
vuex: 4.0.2  

## Dev Tools

Visual Studio Code con: Vetur, StandarJs  
Google Chrome
